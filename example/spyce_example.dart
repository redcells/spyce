import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:spyce/spyce.dart';

class Languages extends Job {
  final type = 'Languages';
  static const decoder = JsonDecoder();
  String owner, repo;

  Languages(this.owner, this.repo)
      : super('https://api.github.com/repos/$owner/$repo/languages');

  @override
  Stream<Pipeline> exec(Service service) async* {
    var languages = decoder.convert((await service['http'].get(name)).body);
    print(languages);
  }

  @override
  Map<String, dynamic> toJson() {
    return {'name': name, 'owner': owner, 'repo': repo};
  }

  Languages.fromJson(Map<String, dynamic> obj)
      : owner = obj['owner'],
        repo = obj['repo'],
        super(
          'https://api.github.com/repos/${obj['owner']}/${obj['repo']}/languages');
}

class NewPipeline extends Pipeline {
  final type = 'NewPipeline';
  final owner, repos;

  NewPipeline(String name, this.owner, this.repos)
      : super.fromJobs(name, () sync* {
    for (var repo in repos) {
      yield Languages(owner, repo['name']);
    }
  }());

  @override
  Map<String, dynamic> toJson() {
    return {'name': name, 'owner': owner, 'repos': repos};
  }

  NewPipeline.fromJson(Map<String, dynamic> obj)
      : owner = obj['owner'],
        repos = obj['repos'],
        super.fromJobs(obj['name'], () sync* {
        for (var repo in obj['repos']) {
          yield Languages(obj['owner'], repo['name']);
        }
      }());
}

class Repos extends Job {
  final type = 'Repos';
  static const decoder = JsonDecoder();
  String owner;

  Repos(this.owner) : super('https://api.github.com/users/$owner/repos');

  @override
  Stream<Pipeline> exec(Service service) async* {
    var repos = decoder.convert((await service['http'].get(name)).body);
    yield NewPipeline(name, owner, repos);
  }

  @override
  Map<String, dynamic> toJson() {
    return {'name': name, 'owner': owner};
  }

  Repos.fromJson(Map<String, dynamic> obj)
      : owner = obj['owner'],
        super('https://api.github.com/users/${obj['owner']}/repos');
}

Future<void> main(List<String> args) async {
  if (args.isEmpty && args[0] != 'server' && args[1] != 'client') {
    print('server or client');
    return;
  }
  if (args[0] == 'server') {
    var spyce = Spyce('spyce');
    var runner = RemoteRunner('localhost:8080', 2, '127.0.0.1', 8080);
    runner.addPipeline('NewPipeline', NewPipeline);
    spyce.addRunner(runner);
    spyce.dumpStream.listen(print);
    spyce.logStream.listen(print);
    spyce.addPipeline(Pipeline.fromJob(Repos('github')));
  } else {
    var service = Service();
    service['http'] = http.Client();
    var runner = Runner('runner', service, 2);
    var spyce = RemoteSpyce(runner);
    spyce.addJob('Repos', Repos);
    spyce.addJob('Languages', Languages);
    await spyce.start(8080);
  }
}
