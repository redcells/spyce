// Copyright 2019 Kina Wakasa
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:convert';
import 'dart:mirrors';

import 'package:grpc/grpc.dart' as grpc;

import 'generated/spyce.pb.dart';
import 'generated/spyce.pbgrpc.dart';
import 'job.dart';
import 'pipeline.dart';
import 'runner.dart';

class RemoteRunner implements Runner {
  final String name;
  final String address;
  final int port;
  final encoder = JsonEncoder();
  final decoder = JsonDecoder();
  final _pipelineMirror = Map<String, ClassMirror>();
  grpc.ClientChannel channel;
  SpyceClient stub;
  int _capacity;
  int _flow = 0;

  @override
  bool get isNotFull => _flow < _capacity;

  RemoteRunner(this.name, capacity, this.address, this.port)
      : _capacity = capacity {
    channel = grpc.ClientChannel(address,
        port: port,
        options: const grpc.ChannelOptions(
            credentials: const grpc.ChannelCredentials.insecure()));
    stub = SpyceClient(channel,
        options: grpc.CallOptions(timeout: Duration(seconds: 30)));
  }

  void addPipeline(String name, Type p) {
    _pipelineMirror[name] = reflectClass(p);
  }

  @override
  Stream<Pipeline> exec(Job job) {
    ++_flow;
    return () async* {
      try {
        var ret = stub.assign(JobRequest()
          ..type = job.type
          ..data = encoder.convert(job));
        await for (var re in ret) {
          if (re.type == 'error') {
            throw re.data;
          }
          yield _pipelineMirror[re.type].newInstance(
              #fromJson, [decoder.convert(re.data)]).reflectee as Pipeline;
        }
      } finally {
        --_flow;
      }
    }();
  }
}
