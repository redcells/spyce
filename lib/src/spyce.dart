// Copyright 2019 Kina Wakasa
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:async';
import 'dart:collection';

import 'job.dart';
import 'pipeline.dart';
import 'runner.dart';

class Spyce {
  final String name;
  final _pipelines = Queue<Pipeline>();
  final _runners = List<Runner>();
  int _runnerIndex = 0;
  StreamController<String> _logController;
  Stream<String> _logStream;
  StreamController<Job> _dumpController;
  Stream<Job> _dumpStream;

  Stream<String> get logStream => _logStream;

  Stream<Job> get dumpStream => _dumpStream;

  Spyce(this.name) {
    _logController = StreamController<String>();
    _logStream = _logController.stream.asBroadcastStream();
    _dumpController = StreamController<Job>();
    _dumpStream = _dumpController.stream.asBroadcastStream();
  }

  void _dump(Job job) {
    _dumpController.add(job);
  }

  void _log(var message) {
    _logController.add(message);
  }

  void addPipeline(Pipeline pipeline) {
    _pipelines.add(pipeline);
    _assignJob();
  }

  void addRunner(Runner runner) {
    _runners.add(runner);
    _assignJob();
  }

  void _assignJob() {
    while (_pipelines.isNotEmpty && _runners.isNotEmpty) {
      Runner runner;
      for (int i = _runnerIndex; i < _runnerIndex + _runners.length; ++i) {
        if (_runners[i % _runners.length].isNotFull) {
          runner = _runners[i % _runners.length];
          _runnerIndex = (i + 1) % _runners.length;
          break;
        }
      }
      if (runner == null) {
        break;
      }
      var pipeline = _pipelines.removeFirst();
      bool hasNext;
      try {
        hasNext = pipeline.moveNext();
      } catch (e) {
        _log('Pipeline ${pipeline.name} throw error ${e}');
      }
      if (hasNext == true) {
        _pipelines.add(pipeline);
        var job = pipeline.current;
        var desc =
            'Job #${pipeline.count} ${job.name} in Pipeline ${pipeline.name} on Runner ${runner.name}';
        _log('Running $desc.');
        runner.exec(job).listen((p) {
          addPipeline(p);
          _assignJob();
        }, onDone: () {
          _log('$desc Done.');
          _assignJob();
        }, onError: (e) {
          _log('$desc throw error ${e}');
          _dump(job);
          _assignJob();
        });
      }
    }
  }
}
