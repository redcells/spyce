// Copyright 2019 Kina Wakasa
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'pipeline.dart';
import 'service.dart';

abstract class Job {
  final type = 'Job';
  final String name;

  Job(this.name);

  Stream<Pipeline> exec(Service service);

  Map<String, dynamic> toJson() {
    return {'name': name};
  }
}
