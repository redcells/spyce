///
//  Generated code. Do not modify.
//  source: spyce.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

import 'dart:async' as $async;
import 'dart:core' as $core show int, String, List;

import 'package:grpc/service_api.dart' as $grpc;

import 'spyce.pb.dart';

export 'spyce.pb.dart';

//  Generated code. Do not modify.
//  source: spyce.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

class SpyceClient extends $grpc.Client {
  static final _$assign = $grpc.ClientMethod<JobRequest, PipelineReply>(
      '/spyce.Spyce/Assign',
      (JobRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => PipelineReply.fromBuffer(value));

  SpyceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseStream<PipelineReply> assign(JobRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$assign, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }
}

abstract class SpyceServiceBase extends $grpc.Service {
  $core.String get $name => 'spyce.Spyce';

  SpyceServiceBase() {
    $addMethod($grpc.ServiceMethod<JobRequest, PipelineReply>(
        'Assign',
        assign_Pre,
        false,
        true,
        ($core.List<$core.int> value) => JobRequest.fromBuffer(value),
        (PipelineReply value) => value.writeToBuffer()));
  }

  $async.Stream<PipelineReply> assign_Pre(
      $grpc.ServiceCall call, $async.Future request) async* {
    yield* assign(call, (await request) as JobRequest);
  }

  $async.Stream<PipelineReply> assign(
      $grpc.ServiceCall call, JobRequest request);
}
