///
//  Generated code. Do not modify.
//  source: spyce.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

import 'dart:core' as $core
    show bool, Deprecated, double, int, List, Map, override, String;

import 'package:protobuf/protobuf.dart' as $pb;

//  Generated code. Do not modify.
//  source: spyce.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name

class JobRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('JobRequest', package: const $pb.PackageName('spyce'))
        ..aOS(1, 'type')
        ..aOS(3, 'data')
        ..hasRequiredFields = false;

  JobRequest() : super();

  JobRequest.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);

  JobRequest.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);

  JobRequest clone() => JobRequest()..mergeFromMessage(this);

  JobRequest copyWith(void Function(JobRequest) updates) =>
      super.copyWith((message) => updates(message as JobRequest));

  $pb.BuilderInfo get info_ => _i;

  static JobRequest create() => JobRequest();

  JobRequest createEmptyInstance() => create();

  static $pb.PbList<JobRequest> createRepeated() => $pb.PbList<JobRequest>();

  static JobRequest getDefault() => _defaultInstance ??= create()..freeze();
  static JobRequest _defaultInstance;

  $core.String get type => $_getS(0, '');

  set type($core.String v) {
    $_setString(0, v);
  }

  $core.bool hasType() => $_has(0);

  void clearType() => clearField(1);

  $core.String get data => $_getS(1, '');

  set data($core.String v) {
    $_setString(1, v);
  }

  $core.bool hasData() => $_has(1);

  void clearData() => clearField(3);
}

class PipelineReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i =
      $pb.BuilderInfo('PipelineReply', package: const $pb.PackageName('spyce'))
        ..aOS(1, 'type')
        ..aOS(3, 'data')
        ..hasRequiredFields = false;

  PipelineReply() : super();

  PipelineReply.fromBuffer($core.List<$core.int> i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromBuffer(i, r);

  PipelineReply.fromJson($core.String i,
      [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY])
      : super.fromJson(i, r);

  PipelineReply clone() => PipelineReply()..mergeFromMessage(this);

  PipelineReply copyWith(void Function(PipelineReply) updates) =>
      super.copyWith((message) => updates(message as PipelineReply));

  $pb.BuilderInfo get info_ => _i;

  static PipelineReply create() => PipelineReply();

  PipelineReply createEmptyInstance() => create();

  static $pb.PbList<PipelineReply> createRepeated() =>
      $pb.PbList<PipelineReply>();

  static PipelineReply getDefault() => _defaultInstance ??= create()..freeze();
  static PipelineReply _defaultInstance;

  $core.String get type => $_getS(0, '');

  set type($core.String v) {
    $_setString(0, v);
  }

  $core.bool hasType() => $_has(0);

  void clearType() => clearField(1);

  $core.String get data => $_getS(1, '');

  set data($core.String v) {
    $_setString(1, v);
  }

  $core.bool hasData() => $_has(1);

  void clearData() => clearField(3);
}
