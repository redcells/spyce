// Copyright 2019 Kina Wakasa
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'job.dart';
import 'pipeline.dart';
import 'service.dart';

class Runner {
  final String name;
  int _capacity;
  int _flow = 0;
  final Service _service;

  bool get isNotFull => _flow < _capacity;

  Runner(this.name, service, capacity)
      : _service = service,
        _capacity = capacity;

  Stream<Pipeline> exec(Job job) {
    ++_flow;
    return () async* {
      try {
        await for (var pipeline in job.exec(_service))
          yield pipeline;
      } finally {
        --_flow;
      }
    }();
  }
}
