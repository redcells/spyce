// Copyright 2019 Kina Wakasa
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:convert';
import 'dart:mirrors';

import 'package:grpc/grpc.dart' as grpc;
import 'package:spyce/src/runner.dart';

import 'generated/spyce.pb.dart';
import 'generated/spyce.pbgrpc.dart';
import 'runner.dart';

class SpyceService extends SpyceServiceBase {
  final encoder = JsonEncoder();
  final decoder = JsonDecoder();
  final _jobMirror = Map<String, ClassMirror>();
  final Runner runner;

  SpyceService(this.runner);

  void addJob(String name, Type j) {
    _jobMirror[name] = reflectClass(j);
  }

  @override
  Stream<PipelineReply> assign(
      grpc.ServiceCall call, JobRequest request) async* {
    var stre = runner.exec(_jobMirror[request.type]
        .newInstance(#fromJson, [decoder.convert(request.data)]).reflectee);
    await for (var ss in stre) {
      yield PipelineReply()
        ..type = ss.type
        ..data = encoder.convert(ss);
    }
  }
}

class RemoteSpyce {
  SpyceService _spyceService;

  RemoteSpyce(Runner runner) {
    _spyceService = SpyceService(runner);
  }

  Future<void> start(int port) {
    final server = new grpc.Server([_spyceService]);
    return server.serve(port: port);
  }

  void addJob(String name, Type j) => _spyceService.addJob(name, j);
}
