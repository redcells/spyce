# Spyce

[![Pub](https://img.shields.io/pub/v/spyce.svg)](https://pub.dartlang.org/packages/spyce)

## Overview

Spyce is a data collection framework for general purpose.
The framework implements high-performance asynchronous pipeline scheduling and exposes an easy-to-use interface.
It can be used for a wide range of purposes, from data mining to monitoring and automated testing.

The name "Spyce" originates from Japanese anime television series "Release the Spyce".
